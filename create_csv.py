import pandas as pd

### CREATE CSV FILE TO SAVE LIMITS ###

d = {'exp_lim':[], 'low_2sig':[], 'low_1sig':[], 'high_1sig':[], 'high_2sig':[]}
df = pd.DataFrame(data=d, columns=['exp_lim','low_2sig','low_1sig','high_1sig','high_2sig'])
df.to_csv('exp_limits.csv', index=False)
