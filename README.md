# dDPJ VBF Exclusion Limits Estimation

Exclusion limits estimation on the branching fraction of H -> 2 dDPJ + MET, assuming Higgs production via VBF.

### Pre-requisites

Specific python libraries are needed to run the code. A good option is to create a python virtual enviroment and install the required dependencies inside.

First, install the `python3-venv` module by doing:
```
python3 -m pip install virtualenv
```
Then, create a virtual environment and activate it:
```
python3 -m venv environment_folder
cd environment_folder/
source bin/activate/
```
If you want to leave the environment, just run `deactivate`.
By default, python environments are created with the `pip` package installed inside. Use this to install all other necessary libraries:
```
python3 -m pip install matplotlib
python3 -m pip install seaborn
python3 -m pip install pyhf
python3 -m pip install mplhep
python3 -m pip install pandas
python3 -m pip install notebook
```

### Running the code

`pyhf_test.py` is a template code based on the pyhf tutorial that uses dummy signal and background yields for a generic hypothesis test.
`pyhf_vbfdpj_singlerun.py` does the same than `pyhf_test.py` but using actual signal and background yields after event selection (i.e., yield for one mass point vs. total background yield) allowing to find an upper limit on the signal strength (`mu`) at a chosen confidence level (here, 95%). A brazilian plot is created to visualise the results.

Both of these codes can be run manually as `python CODE_NAME.py`.

`pyhf_vbfdpj.py` functions similarly than the `singlerun` code but is executed many times in order to find the upper limit in `mu` for different signal yields (i.e., reweighted yields). In each iteration, the expected upper limit in `mu` and its confidence bands are rescaled and saved in a csv file.

To run the code, run the shell script as:
```
source run_analysis.sh
```
This script:
* Creates an only-headers csv file.
* Reads a csv containing the reweighted efficiencies as function of ctau for one mass point.
* Runs `pyhf_vbfdpj.py` for each reweighted efficiency.
* Stores the limit results in the csv file.
* Adds respective ctau values to the csv file.

### Plotting

All the plots are produced using a single jupyter notebook. Run it and edit it by doing:
```
jupyter-notebook plot_limits_vs_ctau.ipynb
```

#### DEPRECATED

**This step is obsolote since csv files are now read using** `pandas.read_csv("csvs/FILENAME")`.

A caveat of this plotting script (in its current status) is that it uses `seaborn.load_dataset()` to load and read csv files. This function is special since it will only load csv files inside a folder named `seaborn-data` that must be created inside your local home folder. In my local machine, its path looks like:
```
/home/richards/seaborn-data/
```
Due to this, copy all the needed input csv files to this folder before using the notebook to plot.

### Useful links
* [`pyhf` tutorial](https://pyhf.github.io/pyhf-tutorial/introduction.html)
* [`pyhf` documentation](https://pyhf.readthedocs.io/en/v0.6.3/)
