import json
import matplotlib.pyplot as plt
import numpy as np
import pyhf
from pyhf.contrib.viz import brazil

import mplhep as hep
plt.style.use([hep.style.ATLAS, {'axes.labelsize': 20}, {'axes.labelpad': 20}, {'axes.titlelocation': 'right'}, ])

import sys
import pandas as pd

#create model
model = pyhf.simplemodels.uncorrelated_background(
    signal=[11.2], bkg=[6.57], bkg_uncertainty=[6.57*0.2] # bkg uncert was 1.6, now assuming fixed total uncert
    #signal=[52920*float(sys.argv[1])], bkg=[2*float(sys.argv[2])], bkg_uncertainty=[2*float(sys.argv[2])*0.4]
)

#model
print("")
print(model) #confirm model was created

#print(json.dumps(model.spec, indent=2)) #model specs

print(f"  channels: {model.config.channels}")
print(f"     nbins: {model.config.channel_nbins}")
print(f"   samples: {model.config.samples}")
print(f" modifiers: {model.config.modifiers}")
print(f"parameters: {model.config.parameters}")
print(f"  nauxdata: {model.config.nauxdata}")
print(f"   auxdata: {model.config.auxdata}")
print("")

#print(model.config.nauxdata, model.config.auxdata) #cross-check auxdata

#print(model.expected_data([1.0, 1.0, 1.0])) #specify parameter values (total yield per bin plus auxdata)
#print(model.expected_data([1.0, 1.0, 1.0], include_auxdata=False))

#separately for data and auxdata
#print(model.expected_actualdata([1.0, 1.0, 1.0]))
#print(model.expected_auxdata([1.0, 1.0, 1.0]))

#parameter ordering
#print(model.config.parameters)
#print(model.config.modifiers)
#print(model.config.par_order) #order in  systematics tensor

#parameter multiplicity
#print(model.config.param_set("uncorr_bkguncrt"))
#print(model.config.param_set("uncorr_bkguncrt").n_parameters)

#parameter values
#print(model.config.suggested_init())
#print("Suggested bounds: ", model.config.suggested_bounds())
#print("Suggested fixed pars: ", model.config.suggested_fixed()) #what parameters should be fixed in fit
#print("")

init_pars = model.config.suggested_init()
print(model.expected_actualdata(init_pars))
print("")

#print(model.config.poi_index)
bkg_pars = init_pars.copy()
bkg_pars[model.config.poi_index] = 0 #setting mu=0 (signal turned off)
#print(model.expected_actualdata(bkg_pars))

### SIMPLE INFERENCE ###

observations = [6.57] + model.config.auxdata  #this is a common pattern!
#observations = [2*float(sys.argv[2])] + model.config.auxdata
print(model.logpdf(pars=bkg_pars, data=observations)) #bkg-only model
print(model.logpdf(pars=init_pars, data=observations)) #sgn+bkg model
print("")

#unconstrained likelihood fit
best_fit_values = pyhf.infer.mle.fit(data=observations, pdf=model)
print("Best fit values: ", best_fit_values)
print("")

### HYPOTHESIS TESTING ###

CLs_obs, CLs_exp = pyhf.infer.hypotest(
    1.0,  # null hypothesis
    [6.57] + model.config.auxdata,
    model,
    test_stat="qtilde",
    return_expected_set=True,
)
print(f"      Observed CLs: {CLs_obs:.4f}")
for expected_value, n_sigma in zip(CLs_exp, np.arange(-2, 3)):
    print(f"Expected CLs({n_sigma:2d} σ): {expected_value:.4f}")

print("")

### UPPER LIMIT SETTING ###

poi_values = np.linspace(0.1, 3, 30)
#poi_values = np.linspace(0.1, 10, 100)
#poi_values = np.linspace(0.1, 20, 200)
obs_limit, exp_limits, (scan, results) = pyhf.infer.intervals.upperlimit(
    observations, model, poi_values, level=0.05, return_results=True
)
print(f"Upper limit (obs): μ = {obs_limit:.4f}")
print(f"Upper limit (exp): μ = {exp_limits[2]:.4f}")
print("")

### BRAZILIAN PLOT ###

fig, ax = plt.subplots()
fig.set_size_inches(10.5, 7)
#ax.set_title("Hypothesis test - VBF dDPJ")

artists = brazil.plot_results(poi_values, results, ax=ax)

hep.atlas.text(' Internal', loc=0)
#plt.legend(loc=1) #choose legend corner
#plt.setp(ax.get_legend().get_texts(), fontsize="10")

#plt.text(1,0.9,'FRVZ model - H $\\rightarrow 2\\gamma_{d} +$ X', fontsize=16)
plt.text(1,0.9,'VBF cDPJ channel', fontsize=16)
plt.text(1,0.85,'BR(H$\\rightarrow 2\\gamma_{d} +$X) = 10%', fontsize=16)
plt.text(1,0.8,'$m_H = 125$ GeV, $m_{\\gamma_d} = 400$ MeV', fontsize=16)
plt.savefig("/home/richards/WorkArea/DPJanalysis/repositories/pyhf_vbfdpj/plots/brazilian_plot_500758.png", format="png", bbox_inches="tight")
plt.show()

### SAVE LIMITS IN CSV FILE ###

#d = {'exp_lim':[exp_limits[2]], 'low_2sig':[exp_limits[0]], 'low_1sig':[exp_limits[1]], 'high_1sig':[exp_limits[3]], 'high_2sig':[exp_limits[4]]}
#df = pd.DataFrame(data=d, columns=['exp_lim','low_2sig','low_1sig','high_1sig','high_2sig'])
#df.to_csv('mu_exp_limits.csv', mode='a', index=False, header=False)
