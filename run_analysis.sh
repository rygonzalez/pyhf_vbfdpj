python create_csv.py

#sgn_events=( $(tail -n +2 /home/richards/WorkArea/DPJanalysis/repositories/DPJVBF_LifetimeReweight/csvs/eff_vs_ctaus_500762.csv | cut -d ',' -f2) )
#sgn_events=( $(tail -n +2 ./input/eff_vs_ctaus_500762.csv | cut -d ',' -f2) )
sgn_events=( $(tail -n +2 ./input/ABCD_isoIDvsDPJtagger_SR_v2/eff_vs_ctaus_500762.csv | cut -d ',' -f2) )

#echo "array of effs  : ${sgn_events[@]}"

#for i in ${sgn_events[@]}; do
#    echo $i
#done

#sgn_events="17.0 11.2"
#echo "$sgn_events"
#bkg_events="6.57" #does not change, not needed

#for i in $sgn_events; do
for i in ${sgn_events[@]}; do
    python pyhf_vbfdpj.py $i 28.6
done

python addctau_csv.py
