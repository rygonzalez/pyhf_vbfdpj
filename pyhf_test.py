import json
import matplotlib.pyplot as plt
import numpy as np
import pyhf
from pyhf.contrib.viz import brazil

#create model
model = pyhf.simplemodels.uncorrelated_background(
    signal=[5.0, 10.0], bkg=[50.0, 60.0], bkg_uncertainty=[5.0, 12.0]
)

#model
print(model) #confirm model was created

#print(json.dumps(model.spec, indent=2)) #model specs

print(f"  channels: {model.config.channels}")
print(f"     nbins: {model.config.channel_nbins}")
print(f"   samples: {model.config.samples}")
print(f" modifiers: {model.config.modifiers}")
print(f"parameters: {model.config.parameters}")
print(f"  nauxdata: {model.config.nauxdata}")
print(f"   auxdata: {model.config.auxdata}")
print("")

#print(model.config.nauxdata, model.config.auxdata) #cross-check auxdata

#print(model.expected_data([1.0, 1.0, 1.0])) #specify parameter values (total yield per bin plus auxdata)
#print(model.expected_data([1.0, 1.0, 1.0], include_auxdata=False))

#separately for data and auxdata
#print(model.expected_actualdata([1.0, 1.0, 1.0]))
#print(model.expected_auxdata([1.0, 1.0, 1.0]))

#parameter ordering
#print(model.config.parameters)
#print(model.config.modifiers)
#print(model.config.par_order) #order in  systematics tensor

#parameter multiplicity
#print(model.config.param_set("uncorr_bkguncrt"))
#print(model.config.param_set("uncorr_bkguncrt").n_parameters)

#parameter values
#print(model.config.suggested_init())
#print(model.config.suggested_bounds())
#print(model.config.suggested_fixed()) #what parameters should be fixed in fit

init_pars = model.config.suggested_init()
#print(model.expected_actualdata(init_pars))

#print(model.config.poi_index)
bkg_pars = init_pars.copy()
bkg_pars[model.config.poi_index] = 0 #setting mu=0 (signal turned off)
#print(model.expected_actualdata(bkg_pars))

### SIMPLE INFERENCE ###

observations = [53.0, 65.0] + model.config.auxdata
print(model.logpdf(pars=bkg_pars, data=observations)) #bkg-only model
print(model.logpdf(pars=init_pars, data=observations)) #sgn+bkg model
print("")

#unconstrained likelihood fit
best_fit_values = pyhf.infer.mle.fit(data=observations, pdf=model)
print(best_fit_values)

### HYPOTHESIS TESTING ###
print("")

CLs_obs, CLs_exp = pyhf.infer.hypotest(
    1.0,  # null hypothesis
    [53.0, 65.0] + model.config.auxdata,
    model,
    test_stat="qtilde",
    return_expected_set=True,
)
print(f"      Observed CLs: {CLs_obs:.4f}")
for expected_value, n_sigma in zip(CLs_exp, np.arange(-2, 3)):
    print(f"Expected CLs({n_sigma:2d} σ): {expected_value:.4f}")

### UPPER LIMIT SETTING ###
print("")

poi_values = np.linspace(0.1, 5, 50)
obs_limit, exp_limits, (scan, results) = pyhf.infer.intervals.upperlimit(
    observations, model, poi_values, level=0.05, return_results=True
)
print(f"Upper limit (obs): μ = {obs_limit:.4f}")
print(f"Upper limit (exp): μ = {exp_limits[2]:.4f}")

print("")

fig, ax = plt.subplots()
fig.set_size_inches(10.5, 7)
ax.set_title("Hypothesis Tests")

artists = brazil.plot_results(poi_values, results, ax=ax)
plt.show()

