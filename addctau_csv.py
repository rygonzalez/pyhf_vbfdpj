import pandas as pd

# ctau values
list = [0.1,0.12,0.14,0.16,0.18,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.2,1.4,1.6,1.8,2,3,4,5,6,7,8,9,10,11,12,13,14]
for i in range(15,10005,5):
   list.append(i)
#print(list)

### ADD CTAU VALUES TO CSV FILE ###

df = pd.read_csv("exp_limits.csv")
df['ctau'] = list
df.to_csv("exp_limits.csv", index=False)
